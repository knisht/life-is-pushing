# *Life is pushing* platformer game.

My very own platformer online game written in Java(Jetty Server) & JavaScript(PIXI framework).  
Game consists of PvP matches that last till death of one of players.   

Controls:   
**A**     -> Move left;  
**D**     -> Move right;  
**Space** -> Jump;  
**Shift** -> Gain significant speed for short period of time;  
Mouse clicks generate blows that push your enemy in certain direction.  

Sometimes power-ups may appear on the map.  
Currently there is two of them:  
*Heal*: Increases player's HP for 1.  
*Shield*: Gives immunity to pushes for certain period of time.   


Build by `./gradlew appStart`  
JDK8 required.  

Good luck!  
