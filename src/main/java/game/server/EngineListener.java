package game.server;

import game.engine.GameMap;
import game.engine.Player;
import game.engine.World;
import game.engine.exception.MapLoadException;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.WebSocketListener;
import org.json.JSONObject;

import java.io.IOException;
import java.util.StringJoiner;

import static java.lang.Thread.sleep;

public class EngineListener implements WebSocketListener {
    private Session outbound;
    private long playerId;
    private GameMap map;
    private int oldHp = 3;

    @Override
    public void onWebSocketBinary(byte[] payload, int offset, int len) {
        /* only interested in text messages */
    }

    @Override
    public void onWebSocketClose(int statusCode, String reason) {
        System.out.println("Connection closed...");
        if (this.outbound != null) {
            endLife();
        }
    }

    @Override
    public void onWebSocketConnect(Session session) {
        System.out.println("Connecting...!");
        this.outbound = session;
    }

    @Override
    public void onWebSocketError(Throwable cause) {
        System.out.println("Error!");
        cause.printStackTrace(System.err);
    }

    @Override
    public void onWebSocketText(String message) {
        if (outbound == null) {
            return;
        }
        if (message.equals("give")) {
            if (!World.players.containsKey(playerId)) {
                try {
                    outbound.getRemote().sendString("lost");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return;
            }
            if (World.maps.get(World.players.get(playerId).getMapId()).getPlayerIds().size() == 1) {
                try {
                    outbound.getRemote().sendString("won");
                    endLife();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return;
            }
            sendGameState();
            return;
        }
        if (message.equals("unload")) {
            endLife();
            return;
        }
        JSONObject obj = new JSONObject(message);
        if (obj.has("type") && obj.get("type").equals("init")) {
            processFieldInit(outbound, (Integer) obj.get("fieldWidth"), (Integer) obj.get("fieldHeight"), (String) obj.get("playerName"));

        }
        if (obj.has("keycode")) {
            World.actions.get(playerId).register(obj.getJSONArray("keycode"));
            sendGameState();
        }
        if (obj.has("mX")) {
            Player player = World.players.get(playerId);
            World.actions.get(playerId).register((Integer) obj.get("mX") / player.width_shrinkage, (Integer) obj.get("mY") / player.height_shrinkage);
        }
    }

    private void sendGameState() {
        Player mp = World.players.get(playerId);
        StringJoiner answer = new StringJoiner(", ", "{\"players\": [", "]");
        World.maps.get(World.players.get(playerId).getMapId()).getPlayerIds()
                .stream().map(playerId -> World.players.get(playerId))
                .map(player -> player.toJSON(mp.width_shrinkage, mp.height_shrinkage, mp.getId() == player.getId()))
                .forEach(answer::add);
        String result = answer.toString();
        StringJoiner blowsJoiner = new StringJoiner(", ", "\"blows\": [", "]");
        if (!World.blows.get(playerId).isEmpty()) {
            World.blows.get(playerId)
                    .stream()
                    .map(blow -> blow.toJSON(World.players.get(playerId).width_shrinkage, World.players.get(playerId).height_shrinkage))
                    .forEach(blowsJoiner::add);
            World.blows.get(playerId).clear();
            result = result + ", " + blowsJoiner.toString();
        }
        if (!World.maps.get(mp.getMapId()).getPowerups().isEmpty()) {
            StringJoiner powerUpJoiner = new StringJoiner(", ", "\"powerUps\": [", "]");
            World.maps.get(mp.getMapId())
                    .getPowerups()
                    .values()
                    .stream()
                    .filter(blowId -> !mp.getProcessedPowerUps().keySet().contains(blowId.getId()))
                    .map(up -> {
                        mp.getProcessedPowerUps().put(up.getId(), up.getId());
                        return up.toJSON(mp.width_shrinkage, mp.height_shrinkage);
                    })
                    .forEach(powerUpJoiner::add);
            result = result + ", " + powerUpJoiner.toString();
        }
        StringJoiner rmJoiner = new StringJoiner(", ", "\"toDel\": [", "]");
        mp.getProcessedPowerUps()
                .keySet()
                .stream()
                .filter(id -> !map.getPowerups().keySet().contains(id))
                .forEach(id -> {
                    mp.getProcessedPowerUps().remove(id);
                    rmJoiner.add(id.toString());
                });
        result = result + ", " + rmJoiner.toString();
        if (oldHp > mp.getHitpoints()) {
            result += ", \"nHp\": " + mp.getHitpoints();
            oldHp--;
        } else if (oldHp < mp.getHitpoints()) {
            oldHp++;
            result += ", \"pHp\": " + mp.getHitpoints();
        }
        if (mp.getChangeShift().equals("disable")) {
            result += ", \"shift\": 0";
        } else
        if (mp.getChangeShift().equals("enable")) {
            result += ", \"shift\": 1";
        }
        if (mp.getChangePush().equals("disable")) {
            result += ", \"push\": 0";
        } else
        if (mp.getChangePush().equals("enable")) {
            result += ", \"push\": 1";
        }
        mp.setChangeShift("");
        result = result + "}";
        try {
            outbound.getRemote().sendString(result);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void endLife() {
        if (World.players.containsKey(playerId)) {
            Player player = World.players.get(playerId);
            long mapId = player.getMapId();
            World.maps.get(mapId).getPlayerIds().remove(playerId);
            World.players.remove(playerId);
            if (World.maps.get(mapId).getPlayerIds().size() == 0) {
                World.manager.endTask(mapId);
                World.maps.remove(mapId);
            }
        }
        this.outbound = null;
    }

    private long selectMap() {
        for (long mapId : World.maps.keySet()) {
            GameMap map = World.maps.get(mapId);
            if (map.getPlayerIds().size() < map.getMaxPlayerAmount()) {
                return mapId;
            }
        }
        try {
            long mapId = World.generateMap();
            World.manager.generateMap(mapId);
            return mapId;
        } catch (MapLoadException e) {
            System.err.println(e.getMessage());
            try {
                sleep(1000);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            return selectMap();
        }
    }


    private void processFieldInit(Session session, int width, int height, String playerName) {
        long mapId = selectMap();
        System.out.println("Here!!!");
        map = World.maps.get(mapId);
        playerId = World.generatePlayer(map, width, height, playerName);
        Player player = World.players.get(playerId);

        World.players.get(playerId).tieWithMap(mapId);
        map.tieWithPlayer(playerId);

        System.out.println(World.maps);
        System.out.println(World.players);

        while (map.getPlayerIds().size() != map.getMaxPlayerAmount()) {
            try {
                sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        map.run();
        StringJoiner sj = new StringJoiner(", ", "{ \"type\": \"initial\", \"playerNames\": [", "]");
        map.getPlayerIds().stream().map(id -> World.players.get(id)).map(pl -> "\"" + pl.getName() + "\"").forEach(sj::add);
        StringJoiner sawJoiner = new StringJoiner(", ", "\"saws\": [", "]");
        map.getSaws().stream().forEach(saw -> sawJoiner.add(saw.toJSON(player.width_shrinkage, player.height_shrinkage)));
        String answer = sj.toString() + ", \"file\": " + "\"" + map.getFilename() + "\", "
                + sawJoiner.toString() + ", "
                + "\"playerId\": " + playerId + ", \"playerHeight\": "
                + (int) (map.getPlayerHeight() * player.height_shrinkage) + ", \"playerWidth\": " +
                +(int) (map.getPlayerWidth() * player.width_shrinkage) + ", \"playersAmount\": "
                + map.getMaxPlayerAmount()
                + ", \"blowDistance\": " + (map.getBlowDistance() * player.width_shrinkage) + "}";
        System.out.println(answer);
        try {
            session.getRemote().sendString(answer);
        } catch (IOException e) {
            System.err.println("RemoteError!!");
            e.printStackTrace();
        }
    }

}