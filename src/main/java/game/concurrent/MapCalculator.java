package game.concurrent;

import game.engine.*;
import game.engine.physics.Processor;

import java.util.List;
import java.util.stream.Collectors;

public class MapCalculator implements Runnable {

    private long mapId;

    MapCalculator(long mapId) {
        this.mapId = mapId;
    }

    @Override
    public void run() {
        Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
        GameMap map = World.maps.get(mapId);
        if (!map.isRunning()) {
            return;
        }
        List<Player> players = map
                .getPlayerIds()
                .stream()
                .map(World.players::get)
                .collect(Collectors.toList());

        if (players.size() != map.getPlayerIds().size()) {
            players = map
                    .getPlayerIds()
                    .stream()
                    .map(World.players::get)
                    .collect(Collectors.toList());
        }
        AbstractPowerup powerup = map.isReadyPowerup();
        if (powerup != null) {
            map.getPowerups().put(powerup.getId(), powerup);
        }
        for (Player player : players) {
            Keycode keycode = World.actions.get(player.getId());
            int resx = 0;
            int resy = 0;
            if (keycode.isLeft()) {
                resx += -World.maps.get(mapId).getSpeedMaxLimit();
            }
            if (keycode.isRight()) {
                resx += World.maps.get(mapId).getSpeedMaxLimit();
            }
            if (keycode.isUp()) {
//                System.out.println("Upping");
                resy = -1;
            }
            if (keycode.isShift()) {
                player.goShift();
            }
            if (keycode.isBlow()) {
                player.castBlow();
            }
            if (keycode.isClicked()) {
                double x = keycode.getPressX();
                double y = keycode.getPressY();
                player.castBlow(x, y);
            }
            player.move(resx, resy);
            Processor.calculatePosition(player);
        }
    }

}
