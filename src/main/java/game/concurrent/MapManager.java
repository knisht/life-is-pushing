package game.concurrent;

import java.util.Map;
import java.util.concurrent.*;

public class MapManager {
    private ScheduledExecutorService service = Executors.newScheduledThreadPool(Thread.activeCount());

    private long delay;
    private long interval;
    private TimeUnit unit;

    private Map<Long, ScheduledFuture<?>> tasks;

    public MapManager(long delay, long interval, TimeUnit unit) {
        this.delay = delay;
        this.interval = interval;
        this.unit = unit;
        this.tasks = new ConcurrentHashMap<>();
    }


    public void generateMap(long id) {
        tasks.put(id, service.scheduleAtFixedRate(new MapCalculator(id), delay, interval, unit));
        System.out.println("Task started for " + id);
    }

    public void endTask(long mapId) {
        tasks.get(mapId).cancel(true);
        tasks.remove(mapId);
        System.out.println("Task ended for" + mapId);
    }
}
