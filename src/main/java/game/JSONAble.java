package game;

public interface JSONAble {
    String toJSON(double xCoefficient, double yCoefficient);
}
