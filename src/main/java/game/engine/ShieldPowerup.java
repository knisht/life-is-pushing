package game.engine;

public class ShieldPowerup extends AbstractPowerup {

    ShieldPowerup(int x, int y, int radius, int id) {
        super(x, y, radius, id, "shield");
    }

    @Override
    public void affect(Player player) {
        player.setBlowImmunity();
    }
}
