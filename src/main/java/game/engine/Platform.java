package game.engine;


import game.JSONAble;

public abstract class Platform extends GameObject implements JSONAble {

    private int x;
    private int y;

    private int width;
    private int height;
    private boolean deadly;

    public Platform(boolean solid, boolean deadly, int x, int y, int width, int height) {
        super(solid);
        this.deadly = deadly;
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
    }

    public double getResistanceCoefficient() {
        return 5;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }

    public String toJSONProto(String type, double xCoefficient, double yCoefficient) {
        return "{\"type\": \"" + type + "\", "
                + "\"x\": " + getX() * xCoefficient
                + ", \"y\": " + getY() * yCoefficient
                + ", \"width\": " + getWidth() * xCoefficient
                + ", \"height\": " + getHeight() * yCoefficient + "}";

    }

    public boolean isDeadly() {
        return deadly;
    }

}
