package game.engine.exception;

public class MapLoadException extends RuntimeException {
    public MapLoadException(String s) {
        super(s);
    }
}
