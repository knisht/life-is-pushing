package game.engine;

public class Spikes extends Platform {


    public Spikes(int x, int y, int width, int height) {
        super(false, true, x, y, width, height);
    }

    @Override
    public String toJSON(double xCoefficient, double yCoefficient) {
        return "{\"type\": \"" + "spikes" + "\", "
                + "\"x\": " + getX() * xCoefficient
                + ", \"y\": " + getY() * yCoefficient
                + ", \"width\": " + getWidth() * xCoefficient
                + ", \"height\": " + getHeight() * yCoefficient
                + "}";
    }

}
