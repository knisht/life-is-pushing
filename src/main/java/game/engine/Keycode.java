package game.engine;

import org.json.JSONArray;

public class Keycode {
    private boolean up = false;
    private boolean readyForUp = true;
    private boolean left = false;
    private boolean right = false;
    private boolean blow = false;
    private boolean shift = false;
    private boolean readyForShift = true;

    private double pressX = -1;
    private double pressY = -1;

    public boolean isUp() {
        if (readyForUp && up) {
            readyForUp = false;
            return true;
        } else {
            return false;
        }
    }


    public boolean isShift() {
        if (readyForShift && shift) {
            readyForShift = false;
            return true;
        } else {
            return false;
        }
    }

    public boolean isLeft() {
        return left;
    }

    public boolean isRight() {
        return right;
    }

    public boolean isBlow() {
        return blow;
    }


    public void register(JSONArray array) {
        for (Object obj : array) {
            int keycode = (Integer) obj;
            if (keycode == 65) {
                left = true;
            } else if (keycode == 68) {
                right = true;
            } else if (keycode == 16) {
                shift = true;
            } else if (keycode == 32) {
                up = true;
            } else if (keycode == 13) {
                blow = true;
            } else if (keycode == -65) {
                left = false;
            } else if (keycode == -16) {
                readyForShift = true;
                shift = false;
            } else if (keycode == -68) {
                right = false;
            } else if (keycode == -32) {
                readyForUp = true;
                up = false;
            } else if (keycode == -13) {
                blow = false;
            }
        }
    }

    public void register(double x, double y) {
        this.pressX = x;
        this.pressY = y;
    }

    public boolean isClicked() {
        return pressX != -1 && pressY != -1;
    }

    public double getPressX() {
        double tmp = pressX;
        pressX = -1;
        return tmp;
    }

    public double getPressY() {
        double tmp = pressY;
        pressY = -1;
        return tmp;
    }
}
