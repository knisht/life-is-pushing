package game.engine;

import game.JSONAble;

public abstract class AbstractPowerup extends GameObject implements JSONAble {
    private int x;
    private int y;
    private int id;
    private int radius;
    private String type;

    AbstractPowerup(int x, int y, int radius, int id, String type) {
        super(false);
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.id = id;
        this.type = type;
    }


    @Override
    public String toJSON(double xCoefficient, double yCoefficient) {
        return "{\"" + type + "\": " + id + ", \"x\": " + Math.round(x * xCoefficient) + ", \"y\": " + Math.round(y * yCoefficient) + "}";
    }

    public int getRadius() {
        return radius;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public int getId() {
        return id;
    }

    public abstract void affect(Player player);
}

