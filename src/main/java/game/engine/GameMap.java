package game.engine;

import game.engine.physics.Force;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class GameMap {

    private List<Platform> platforms;
    private List<Saw> saws;
    private String name;
    private int width;
    private int height;
    private String filename;
    private int maxJumpsAmount;
    private int blowForce;
    private long timeUnderBlow;
    private long blowCooldown;
    private double blowDistance;
    private long damageThreshold;
    private Force gravityForce;
    private Force reactionForce;
    private double hForceSlowRate;
    private double vForceSlowRate;
    private int powerUpInterval;
    private int playerWidth;
    private int playerHeight;
    private double jumpSpeed;
    private int moveForceAmount;
    private double speedMinLimit;
    private double speedMaxLimit;
    private long shiftCooldown;
    private int powerUpRadius;
    private double blowSpeedMaxLimit;
    private int maxPlayerAmount;
    private long blowFunctionThreshold;
    private List<Long> playerIds;
    private List<Map.Entry<Integer, Integer>> positions;
    private int passedIterations = 0;
    private Map<Integer, AbstractPowerup> powerups;
    private boolean running = false;
    private int powerupIds = 0;
    private int shiftSpeed;
    private long immunityThreshold;
    private int invisibilityThreshold;

    GameMap(JSONObject target) {
        platforms = new CopyOnWriteArrayList<>();
        playerIds = new CopyOnWriteArrayList<>();
        positions = new CopyOnWriteArrayList<>();
        powerups = new ConcurrentHashMap<>();
        saws = new CopyOnWriteArrayList<>();

        name = target.getString("name");
        filename = target.getString("filename");
        width = target.getInt("width");
        height = target.getInt("height");
        maxJumpsAmount = target.getInt("maxJumpsAmount");
        blowForce = target.getInt("blowForce");
        shiftSpeed = target.getInt("shiftSpeed");
        timeUnderBlow = target.getLong("timeUnderBlow");
        blowCooldown = target.getLong("blowCooldown");
        blowDistance = target.getInt("blowDistance");
        powerUpRadius = target.getInt("powerUpRadius");
        powerUpInterval = target.getInt("powerUpInterval");
        damageThreshold = target.getInt("damageReceiveInterval");
        gravityForce = new Force(0, target.getInt("gravityForce"));
        reactionForce = new Force(0, target.getInt("reactionForce"));
        hForceSlowRate = target.getDouble("hForceSlowRate");
        vForceSlowRate = target.getDouble("vForceSlowRate");
        playerWidth = target.getInt("playerWidth");
        playerHeight = target.getInt("playerHeight");
        invisibilityThreshold = target.getInt("invisibilityThreshold");
        shiftCooldown = target.getInt("shiftCooldown");
        immunityThreshold = target.getLong("immunityThreshold");
        jumpSpeed = target.getDouble("jumpSpeed");
        moveForceAmount = target.getInt("moveForceAmount");
        speedMaxLimit = target.getDouble("speedMaxLimit");
        speedMinLimit = target.getDouble("speedMinLimit");
        blowSpeedMaxLimit = target.getDouble("blowSpeedMaxLimit");
        maxPlayerAmount = target.getInt("maxPlayerAmount");
        blowFunctionThreshold = target.getInt("blowFunctionThreshold");
        for (Object obj : target.getJSONArray("platforms")) {
            JSONObject platform = (JSONObject) obj;
            platforms.add(new SolidPlatform(platform.getInt("x"), platform.getInt("y"), platform.getInt("width"), platform.getInt("height")));
        }
        for (Object obj : target.getJSONArray("spikes")) {
            JSONObject spike = (JSONObject) obj;
            platforms.add(new Spikes(spike.getInt("x"), spike.getInt("y"), spike.getInt("width"), spike.getInt("height")));
        }
        for (Object obj : target.getJSONArray("playerPositions")) {
            JSONObject position = (JSONObject) obj;
            positions.add(new HashMap.SimpleEntry<>(position.getInt("x"), position.getInt("y")));
        }
        for (Object obj : target.getJSONArray("saws")) {
            JSONObject saw = (JSONObject) obj;
            saws.add(new Saw(saw.getInt("x"), saw.getInt("y"), saw.getInt("radius")));
        }

    }

    public int getMaxJumpsAmount() {
        return maxJumpsAmount;
    }

    public int getBlowForce() {
        return blowForce;
    }

    public long getTimeUnderBlow() {
        return timeUnderBlow;
    }

    public long getBlowCooldown() {
        return blowCooldown;
    }

    public double getBlowDistance() {
        return blowDistance;
    }

    public long getDamageThreshold() {
        return damageThreshold;
    }

    public Force getGravityForce() {
        return gravityForce;
    }

    public Force getReactionForce() {
        return reactionForce;
    }

    public double gethForceSlowRate() {
        return hForceSlowRate;
    }

    public double getvForceSlowRate() {
        return vForceSlowRate;
    }

    public int getPlayerWidth() {
        return playerWidth;
    }

    public int getPlayerHeight() {
        return playerHeight;
    }

    public double getJumpSpeed() {
        return jumpSpeed;
    }

    public int getMoveForceAmount() {
        return moveForceAmount;
    }

    public double getSpeedMinLimit() {
        return speedMinLimit;
    }

    public double getSpeedMaxLimit() {
        return speedMaxLimit;
    }

    public double getBlowSpeedMaxLimit() {
        return blowSpeedMaxLimit;
    }

    public int getMaxPlayerAmount() {
        return maxPlayerAmount;
    }

    public long getBlowFunctionThreshold() {
        return blowFunctionThreshold;
    }


    public List<Platform> getPlatforms() {
        return platforms;
    }

    public void init() {

    }


    public void tieWithPlayer(long playerId) {
        playerIds.add(playerId);
    }

    public List<Long> getPlayerIds() {
        return playerIds;
    }

    public int getMapWidth() {
        return width;
    }

    public int getMapHeight() {
        return height;
    }

    public String getFilename() {
        return filename;
    }

    public String getName() {
        return name;
    }

    public List<Map.Entry<Integer, Integer>> getPositions() {
        return positions;
    }

    public List<Saw> getSaws() {
        return saws;
    }

    public AbstractPowerup isReadyPowerup() {
        ++passedIterations;
        if (passedIterations % powerUpInterval == 0) {
            ++powerupIds;
            int choose = (int) Math.round(Math.random()*100);
            if (choose < 33) {
                return new HealthPowerup((int) Math.round(Math.random() * width), (int) Math.round(Math.random() * height), this.powerUpRadius, powerupIds);
            } else if (choose < 66) {
                return new ShieldPowerup((int) Math.round(Math.random() * width), (int) Math.round(Math.random() * height), this.powerUpRadius, powerupIds);
            } else {
                return new InvisibilityPowerup((int) Math.round(Math.random() * width), (int) Math.round(Math.random() * height), this.powerUpRadius, powerupIds);
            }
        } else {
            return null;
        }
    }

    public Map<Integer, AbstractPowerup> getPowerups() {
        return powerups;
    }

    public void run() {
        this.running = true;
    }

    public boolean isRunning() {
        return running;
    }

    public long getShiftCooldown() {
        return shiftCooldown;
    }

    public int getShiftSpeed() {
        return shiftSpeed;
    }

    public long getImmunityThreshold() {
        return immunityThreshold;
    }

    public int getInvisibilityThreshold() {
        return invisibilityThreshold;
    }
}
