package game.engine;

public class SolidPlatform extends Platform {
    public SolidPlatform(int x, int y, int width, int height) {
        super(true, false, x, y, width, height);
    }

    @Override
    public String toJSON(double xCoefficient, double yCoefficient) {
        return toJSONProto("solid", xCoefficient, yCoefficient);
    }
}
