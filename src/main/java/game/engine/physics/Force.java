package game.engine.physics;

public class Force extends Vector {

    public Force(double xProjection, double yProjection) {
        super(xProjection, yProjection);
    }


    public Force add(Force other) {
        return new Force(this.getX() + other.getX(), this.getY() + other.getY());
    }

}
