package game.engine.physics;

public abstract class Vector {
    private double x;
    private double y;

    public Vector(double xProjection, double yProjection) {
        this.x = xProjection;
        this.y = yProjection;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void set(double xProjection, double yProjection) {
        this.x = xProjection;
        this.y = yProjection;
    }

    public Force multiply(double scalar) {
        return new Force(x * scalar, y * scalar);
    }

    public double getModule() {
        return Math.sqrt(getX() * getX() + getY() * getY());
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }
}
