package game.engine.physics;

public class Acceleration extends Vector {

    public Acceleration(double xProjection, double yProjection) {
        super(xProjection, yProjection);
    }

    public Acceleration(Force f, double weight) {
        super(f.getX() / weight, f.getY() / weight);
    }
}
