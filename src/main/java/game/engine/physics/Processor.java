package game.engine.physics;

import game.engine.*;

import java.util.HashMap;
import java.util.Map;

public class Processor {

    public static void calculatePosition(Player player) {
        GameMap map = World.maps.get(player.getMapId());
        if (Math.abs(player.getMoveForce().getY()) < 1) {
            player.getMoveForce().setY(0);
        } else {
            player.getMoveForce().setY(player.getMoveForce().getY() * map.getvForceSlowRate());
        }
        Force response = triggerCollisions(map, player);
        Force resultantForce = player.getMoveForce()
                .add(response)
                .add(map.getGravityForce());
        Acceleration acc = new Acceleration(resultantForce, player.getWeight());
        Velocity v = player.getVelocity();
        double time = 0.75;
        v.setY(v.getY() + time * acc.getY());
    }


    private static void stabilize(Player player, GameMap map, Platform platform, Connection conn) {
        if (conn != Connection.DOWN) {
//            System.out.println(conn);
        }
        if (conn == Connection.RIGHT) {
            player.setX(player.getOldX());
        }
        if (conn == Connection.LEFT) {
            player.setX(player.getOldX());
        }
        if (conn == Connection.DOWN && player.getVelocity().getY() > 0) {
            player.setY(platform.getY() - map.getPlayerHeight());
        }
        if (conn == Connection.UP && player.getVelocity().getY() < 0) {
            player.setY(platform.getY() + platform.getHeight());
        }
    }

    private static Connection collide(Player player, Platform platform, GameMap map) {

        if (player.getX() + map.getPlayerWidth() / 2 >= platform.getX()
                && player.getX() <= platform.getX() + platform.getWidth()
                && player.getY() <= platform.getY()
                && player.getY() + map.getPlayerHeight() >= platform.getY()) {
            return Connection.DOWN;
        }

        if (player.getX() + map.getPlayerWidth() >= platform.getX()
                && player.getX() <= platform.getX() + platform.getWidth()
                && player.getY() <= platform.getY() + platform.getHeight()
                && player.getY() + map.getPlayerHeight() >= platform.getY() + platform.getHeight()) {
            return Connection.UP;
        }

        if (player.getY() + map.getPlayerHeight() <= platform.getY() + platform.getHeight()
                && player.getY() >= platform.getY()
                && player.getX() >= platform.getX()
                && player.getX() <= platform.getX() + platform.getWidth()) {
            return Connection.RIGHT;
        }
        if (player.getY() + map.getPlayerHeight() <= platform.getY() + platform.getHeight()
                && player.getY() >= platform.getY()
                && player.getX() + map.getPlayerWidth() >= platform.getX()
                && player.getX() + map.getPlayerWidth() <= platform.getX() + platform.getWidth()) {
            return Connection.LEFT;
        }
        return Connection.NONE;
    }

    private static Force triggerCollisions(GameMap map, Player player) {
        Force answer = new Force(0, 0);
        for (Platform platform : map.getPlatforms()) {
            Connection connection = collide(player, platform, map);
            if (connection.equals(Connection.NONE)) {
                continue;
            }
//            System.out.println(connection);
            if (platform.isSolid()) {
                stabilize(player, map, platform, connection);
                player.blockMove(connection);
                if (connection.equals(Connection.DOWN)) {
                    player.getVelocity().setY(0.1);
                    player.setJumpsDone(0);
                }
                if (connection.equals(Connection.UP)) {
                    player.getVelocity().setY(10);
                }
            }
            if (platform.isDeadly()) {
                player.receiveDamage();
            }
        }
        for (Saw saw : map.getSaws()) {
            if (dist(player.getX() + map.getPlayerWidth() / 2.0, player.getY() + map.getPlayerHeight() / 2.0, saw.getX(), saw.getY())
                    < saw.getRadius() + (map.getPlayerHeight() + map.getPlayerWidth()) / 2) {
                player.receiveDamage();
            }
        }
        for (AbstractPowerup powerup : map.getPowerups().values()) {
            if (dist(player.getX() + map.getPlayerWidth() / 2.0, player.getY() + map.getPlayerHeight() / 2.0, powerup.getX(), powerup.getY())
            < powerup.getRadius() + (map.getPlayerHeight() + map.getPlayerWidth()) / 2) {
                powerup.affect(player);
                map.getPowerups().remove(powerup.getId());
            }
        }
        return answer;
    }

    private static double dist(double x1, double y1, double x2, double y2) {
        return Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    }

    private static Map.Entry<Double, Double> circleProjection(double circleX, double circleY, double circleLim, double dstX, double dstY) {
        double dist = dist(circleX, circleY, dstX, dstY);
        double dx = (dstX - circleX) / dist;
        double dy = (dstY - circleY) / dist;
        return new HashMap.SimpleEntry<>(dx * circleLim, dy * circleLim);

    }

    public static Blow generateBlow(GameMap map, Player player, double x, double y) {
        double dist = dist(x, y, player.getX(), player.getY());
        return doBlow(map, player, (x - player.getX()) / dist, (y - player.getY()) / dist);
    }

    public static Blow generateBlow(GameMap map, Player player) {
        double angle = Math.signum(player.getVelocity().getX());
        angle = (angle == 0) ? 1 : 0;
        return doBlow(map, player, 0, angle);
    }

    private static Blow doBlow(GameMap map, Player player, double xPr, double yPr) {

        double angle = Math.asin(yPr);
        angle = (xPr < 0) ? Math.PI - angle : angle;
        for (Long enemyId : map.getPlayerIds()) {
            if (enemyId.equals(player.getId())) {
                continue;
            }
            Player enemy = World.players.get(enemyId);
            double dist = dist(player.getX(), player.getY(), enemy.getX(), enemy.getY());

            if (dist <= map.getBlowDistance()) {
                Map.Entry<Double, Double> enemyProjection = circleProjection(player.getX(),
                        player.getY(),
                        map.getBlowDistance(),
                        enemy.getX(),
                        enemy.getY());
                double enemyAngle = Math.asin(enemyProjection.getKey() / map.getBlowDistance());
                enemyAngle = (xPr < 0) ? Math.PI - enemyAngle : enemyAngle;
                double diff = enemyAngle - angle;
                if (diff < Math.PI / 4 || diff > 3 * Math.PI / 4) {
                    continue;
                }
                enemy.receiveBlow(new Force(xPr, yPr).multiply(map.getBlowForce()).multiply((map.getBlowFunctionThreshold() / (map.getBlowFunctionThreshold() + Math.pow(dist, 1.2)))));
            }
        }
        return new Blow(angle, player.getX() + (map.getPlayerWidth() / 2.0), player.getY() + (map.getPlayerHeight() / 2.0));

    }

    public enum Connection {
        UP, DOWN, LEFT, RIGHT, NONE
        //TODO: create removement of old desktop screen
    }
}
