package game.engine;

public class InvisibilityPowerup extends AbstractPowerup {
    InvisibilityPowerup(int x, int y, int radius, int id) {
        super(x, y, radius, id, "invisible");
    }

    @Override
    public void affect(Player player) {
        player.setInvisibility();
    }
}
