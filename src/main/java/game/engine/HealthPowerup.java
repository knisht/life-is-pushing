package game.engine;

public class HealthPowerup extends AbstractPowerup {

    HealthPowerup(int x, int y, int radius, int id) {
        super(x, y, radius, id, "health");
    }

    @Override
    public void affect(Player player) {
        player.receiveHitpoints(1);
    }
}
