package game.engine;

import game.JSONAble;
import game.engine.physics.Force;
import game.engine.physics.Processor;
import game.engine.physics.Velocity;
import jdk.nashorn.internal.runtime.WithObject;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Player extends GameObject {

    private final long id;
    public double width_shrinkage;
    public double height_shrinkage;
    private long lastTimeDamaged;
    private int x;
    private int y;
    private int oldX;
    private int oldY;
    private Velocity v;
    private Force moveForce;
    private long mapId;
    private int hitPoints;
    private long lastTimeBlowed;
    private double weight;
    private String name;
    private int width;
    private int height;
    private int jumpsDone;
    private boolean blocked = false;
    private double xBlowSpeed = 0;
    private Map<Integer, Integer> processedPowerUps;
    private long lastTimeShifted = 0;
    private int shift;
    private String changeShift;
    private boolean isShiftTracked = false;
    private String changePush;
    private boolean isPushTracked = false;
    private long blowImmunityTime = 0;
    private int invisibilityTime = 0;


    Player(int x, int y, int width, int height, long id, String name) {
        super(true);
        this.x = x;
        this.processedPowerUps = new ConcurrentHashMap<>();
        this.id = id;
        this.y = y;
        this.jumpsDone = 0;
        this.name = name;
        this.width = width;
        this.height = height;
        this.width_shrinkage = 1;
        this.height_shrinkage = 1;
        this.weight = 50;
        this.lastTimeDamaged = System.nanoTime();
        this.lastTimeBlowed = 0;
        this.v = new Velocity(0, 0);
        this.moveForce = new Force(0.0, 0);
        this.hitPoints = 3;
    }

    public void move(int dx, int dy) {
        if (dy < 0 && this.jumpsDone < World.maps.get(mapId).getMaxJumpsAmount()) {
            this.jumpsDone += 1;
            this.v.setY(-World.maps.get(mapId).getJumpSpeed());
        }
        long now = System.nanoTime();
        if (!isShiftTracked && now - lastTimeShifted > World.maps.get(mapId).getShiftCooldown()) {
            changeShift = "enable";
            isShiftTracked = true;
        }
        if (!isPushTracked && now - lastTimeBlowed > World.maps.get(mapId).getBlowCooldown()) {
            changePush = "enable";
            isPushTracked = true;
        }
        double dv = this.v.getY();
        this.y += dv;
        this.oldX = x;
        --blowImmunityTime;
        --invisibilityTime;
        if (!blocked) {
            this.x += dx + xBlowSpeed + Math.signum(dx) * shift;
        }
        if (shift!=0) {
            shift *= 0.9;
            if (shift < 1) {
                shift = 0;
            }
        }
        if (xBlowSpeed != 0) {
            xBlowSpeed *= 0.9;
//            if (xBlowSpeed != 0) System.out.println(xBlowSpeed);
            if (Math.abs(xBlowSpeed) < 1) {
                xBlowSpeed = 0;
            }
        }
        blocked = false;
    }

    public void receiveBlow(Force other) {
        if (blowImmunityTime >= 0) {
            return;
        }
        System.out.println("Force = " + other);
        this.moveForce = this.moveForce.add(other);
        this.xBlowSpeed = other.getX();
    }

    public String toJSON(double xCoefficient, double yCoefficient, boolean me) {
        if (invisibilityTime >=0 && !me) {
            System.out.println("invisibilityTime: " + invisibilityTime);
            return "{\"hp\": " + hitPoints
                    + ", \"invisible\": true}";
        }
        return "{\"hp\": " + hitPoints
                + ", \"x\": " + (int) (getX() * xCoefficient)
                + ", \"y\": " + (int) (getY() * yCoefficient)
                + ((shift!=0) ? ", \"shifting\": 0" : "")
                + (isBlowImmunity() ? ", \"blowImmunity\": 0" : "")
                + (invisibilityTime >=0 ? ", \"invisibleMe\": 1" : "")
                + "}";
    }


    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Velocity getVelocity() {
        return v;
    }

    public double getWeight() {
        return this.weight;
    }

    public Force getMoveForce() {
        return moveForce;
    }

    public void tieWithMap(long mapId) {
        this.mapId = mapId;
        this.width_shrinkage = width / (1.0 * World.maps.get(mapId).getMapWidth());
        this.height_shrinkage = height / (1.0 * World.maps.get(mapId).getMapHeight());
    }

    public long getMapId() {
        return mapId;
    }

    public void receiveDamage() {
        long now = System.nanoTime();
        if (now - lastTimeDamaged > World.maps.get(mapId).getDamageThreshold()) {
            this.hitPoints -= 1;
            this.lastTimeDamaged = now;
        }
        if (this.hitPoints == 0) {
            World.maps.get(mapId).getPlayerIds().remove(id);
            World.players.remove(id);
        }
    }

    public void castBlow() {
        long now = System.nanoTime();
        if (now - lastTimeBlowed > World.maps.get(mapId).getBlowCooldown()) {
            lastTimeBlowed = now;
            Blow blow = Processor.generateBlow(World.maps.get(this.mapId), this);
            for (Long id : World.maps.get(mapId).getPlayerIds()) {
                World.blows.get(id).add(blow);
            }
        }
    }

    public void castBlow(double x, double y) {
        long now = System.nanoTime();
        if (now - lastTimeBlowed > World.maps.get(mapId).getBlowCooldown()) {
            lastTimeBlowed = now;
            changePush = "disable";
            isPushTracked = false;
            Blow blow = Processor.generateBlow(World.maps.get(this.mapId), this, x, y);
            for (Long id : World.maps.get(mapId).getPlayerIds()) {
                World.blows.get(id).add(blow);
            }
        }
    }


    public long getId() {
        return id;
    }

    public void setJumpsDone(int jumpsDone) {
        this.jumpsDone = jumpsDone;
    }

    public String getName() {
        return this.name;
    }

    public void blockMove(Processor.Connection connection) {
        switch (connection) {
            case LEFT:
                this.blocked = true;
                break;
            case RIGHT:
                this.blocked = true;
                break;
            default:
                break;
        }
    }

    public int getHitpoints() {
        return hitPoints;
    }

    public Map<Integer, Integer> getProcessedPowerUps() {
        return processedPowerUps;
    }

    public void receiveHitpoints(int amount) {
        this.hitPoints += amount;
    }

    public void goShift() {
        long now = System.nanoTime();
        if (now - lastTimeShifted > World.maps.get(mapId).getShiftCooldown()) {
            System.out.println("Shifting");
            lastTimeShifted = now;
            shift = World.maps.get(mapId).getShiftSpeed();
            changeShift = "disable";
            isShiftTracked = false;
        }
    }

    public void setxBlowSpeed(double x) {
        xBlowSpeed = x;
    }

    public int getOldX() {
        return oldX;
    }

    public String getChangeShift() {
        return changeShift;
    }

    public void setChangeShift(String s) {
        changeShift = s;
    }

    public String getChangePush() {
        return changePush;
    }
    public void setChangePush(String s) {
        changePush = s;
    }

    void setBlowImmunity() {
        blowImmunityTime = World.maps.get(mapId).getImmunityThreshold();
    }
    private boolean isBlowImmunity() {
        return blowImmunityTime >=0 ;
    }

    void setInvisibility() {
        invisibilityTime = World.maps.get(mapId).getInvisibilityThreshold();
    }
}
