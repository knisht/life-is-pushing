package game.engine;

import game.JSONAble;

public class Saw extends GameObject implements JSONAble {
    private int x;
    private int y;
    private int radius;

    public Saw(int x, int y, int radius) {
        super(false);
        this.x = x;
        this.y = y;
        this.radius = radius;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getRadius() {
        return radius;
    }

    @Override
    public String toJSON(double xCoefficient, double yCoefficient) {
        return "{ \"x\": " + (int) (x * xCoefficient)
                + ", \"y\": " + (int) (y * yCoefficient)
                + ", \"radius\": " + (int) (radius * Math.max(xCoefficient, yCoefficient)) + "}";
    }
}
