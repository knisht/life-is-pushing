package game.engine;

import game.concurrent.MapManager;
import game.engine.physics.Force;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

public class World {
    public static Map<Long, Player> players;
    public static Map<Long, GameMap> maps;
    public static Map<Long, Keycode> actions;
    public static Map<Long, List<Blow>> blows;
    public static MapManager manager = new MapManager(1_000_000, 16666, TimeUnit.MICROSECONDS);
    private static long playerCount;
    private static long mapCount;

    static {
        players = new ConcurrentHashMap<>();
        maps = new ConcurrentHashMap<>();
        actions = new ConcurrentHashMap<>();
        blows = new ConcurrentHashMap<>();
    }

    private static double signum(double x) {
        if (x < 0.0) return -1;
        if (x > 0.0) return 1;
        return 0;
    }

    public static Force getBasicFriction(Player player) {
        return new Force((-1) * Math.signum(player.getVelocity().getX())
                * (player.getWeight()
                * World.maps.get(player.getMapId()).getReactionForce().getModule()), 0);
    }

    public static long generatePlayer(GameMap map, int width, int height, String name) {
        long id = playerCount++;
        actions.put(id, new Keycode());
        int index = map.getPlayerIds().size();
        players.put(id, new Player(map.getPositions().get(index).getKey(), map.getPositions().get(index).getValue(), width, height, id, name));
        blows.put(id, new ArrayList<>());
        return id;
    }

    public static long generateMap() {
        long id = mapCount++;
        maps.put(id, MapLoader.loadMap("face"));
        return id;
    }

}
