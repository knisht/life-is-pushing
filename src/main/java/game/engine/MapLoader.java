package game.engine;

import game.engine.exception.MapLoadException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static java.nio.file.StandardOpenOption.READ;


public class MapLoader {

    private static String root = "src/main/resources/maps/";

    public static GameMap loadMap(String name) {
        //System.out.println(System.getProperty("user.dir"));
        Path filePath = Paths.get(root + name + ".json");

        try (AsynchronousFileChannel fileChannel = AsynchronousFileChannel.open(filePath, READ)) {
            ByteBuffer buffer = ByteBuffer.allocate(4096);
            Future<Integer> operation = fileChannel.read(buffer, 0);
            operation.get();
            String fileContent = new String(buffer.array()).trim();
            buffer.clear();

            JSONObject parsedMap = new JSONObject(fileContent);
            return new GameMap(parsedMap);
        } catch (IOException e) {
            e.printStackTrace();
            throw new MapLoadException("Map not found: " + name);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            throw new MapLoadException("Future::get failed while was reading map: " + name);
        } catch (JSONException e) {
            System.err.println(e.getMessage());
            throw new MapLoadException("Incorrect map format. Please, use JSON in map " + name);
        }

    }

}
