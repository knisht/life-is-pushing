package game.engine;

import game.JSONAble;

public class Blow implements JSONAble {

    private double angle;
    private double x;
    private double y;

    public Blow(double angle, double x, double y) {
        this.x = x;
        this.y = y;
        this.angle = angle;
    }

    @Override
    public String toJSON(double xCoefficient, double yCoefficient) {
        return "{\"rotation\": " + angle
                + ", \"x\": " + x * xCoefficient
                + ", \"y\": " + y * yCoefficient
                + "}";
    }
}
