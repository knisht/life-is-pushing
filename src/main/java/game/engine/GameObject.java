package game.engine;

public class GameObject {
    private boolean solid;

    public GameObject(boolean isSolid) {
        this.solid = isSolid;
    }

    public boolean isSolid() {
        return solid;
    }
}
