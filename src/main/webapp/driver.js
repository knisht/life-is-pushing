window.onkeydown = function (event) {
    if (STATE.game.activeGreeting && event.which < 32) {
        STATE.game.keyLogger.registerKey(event.which);
        if (event.which === 13) {
            endGreeting();
        }
    }
    if (STATE.game.encounterActive) {
        STATE.game.controller.register(event.which);
    }
    if (STATE.game.endgame) {
        console.log("Endgame detected, flushing");
        STATE.game.flush();
    }
};

window.onkeypress = function () {
    if (STATE.game.activeGreeting) {
        STATE.game.keyLogger.registerKey(event.which);
    }
    if (STATE.game.activeTutorial) {
        STATE.game.activeTutorial = false;
        STATE.game.activeGreeting = true;
        STATE.graphics.pixiApp.stage.removeChildren();
        roundInitialSetup();
    }
};

window.onkeyup = function (event) {
    if (STATE.game.encounterActive) {
        STATE.game.controller.register(-event.which);
    }

};

window.onbeforeunload = (function () {
    if (!STATE.game.activeGreeting) {
        STATE.server.sendMessage("unload");
    }
});

let lastTime = 0;


let serverCallback = function (ev) {
    if (ev.data === "lost" || ev.data === "won") {
        if (ev.data === "lost") {
            STATE.game.resultMessage = "lose.svg";
        } else {
            STATE.game.resultMessage = "win.svg";
        }
        STATE.game.encounterActive = false;
        STATE.game.endgame = true;
        STATE.server.connection.close();
        STATE.graphics.pixiApp.stage.removeChildren();
        STATE.graphics.appendGraphicalElement(new EndgameScreen(STATE.game.resultMessage));
        return;
    }

    let data = JSON.parse(ev.data);
    if (data.type === "initial") {
        STATE.graphics.pixiApp.stage.removeChildren();
        STATE.game.hpBar = new HpBar(STATE.graphics.screenWidth / 2 - 25, 80, 50, 2);
        let back = new STATE.graphics.Sprite(STATE.graphics.accessImageTexture('greeting.svg'));
        back.width = STATE.graphics.screenWidth;
        back.height = STATE.graphics.screenHeight;
        STATE.graphics.appendGraphicalElement(back);
        for (let i in data.saws) {
            STATE.game.saws.push(new Saw(data.saws[i].x, data.saws[i].y, data.saws[i].radius));
            STATE.graphics.appendGraphicalElement(STATE.game.saws[i].graphical);
        }
        STATE.graphics.background = new Background(data.file);
        STATE.graphics.appendGraphicalElement(STATE.graphics.background);
        STATE.graphics.appendGraphicalElement(STATE.game.hpBar.graphical);
        for (let i = 0; i < data.playersAmount; ++i) {
            STATE.game.players.push(new Player(0, 0, data.playerWidth, data.playerHeight, data.playerNames[i]));
            STATE.graphics.appendGraphicalElement(STATE.game.players[i].graphical);
        }
        STATE.graphics.appendGraphicalElement(STATE.game.blows);
        STATE.game.shiftIcon = new Icon("shift.svg", 0);
        STATE.graphics.appendGraphicalElement(STATE.game.shiftIcon.graphical);
        STATE.game.pushIcon = new Icon("push.svg", 1);
        STATE.graphics.appendGraphicalElement(STATE.game.pushIcon.graphical);
        STATE.game.activeMapFinding = false;
        STATE.game.blowLim = data.blowDistance;
        STATE.server.sendMessage("give");
        STATE.game.encounterActive = true;
    } else {
        lastTime = Date.now();
        for (let i in data.players) {
            STATE.game.hpBar.notify(STATE.game.players[i].hp() + '', i);
            if (data.players[i].hasOwnProperty('invisible')) {
                STATE.game.players[i].setVisible(false);
                continue;
            } else {
                STATE.game.players[i].setVisible(true);
            }
            STATE.game.players[i].relocate(data.players[i].x, data.players[i].y);
            STATE.game.players[i].renewHp(data.players[i].hp);
            if (data.players[i].hasOwnProperty('shifting')) {
                STATE.game.players[i].setShift(true);
            } else {
                STATE.game.players[i].setShift(false)
            }
            if (data.players[i].hasOwnProperty('blowImmunity')) {
                STATE.game.players[i].setBlowImmunity(true)
            } else {
                STATE.game.players[i].setBlowImmunity(false);
            }
            if (data.players[i].hasOwnProperty('invisibleMe')) {
                STATE.game.players[i].enableInvisibility(true);
            } else {
                STATE.game.players[i].enableInvisibility(false);
            }
        }
        for (let i in data.blows) {
            genBlow(data.blows[i].x, data.blows[i].y, data.blows[i].rotation);
        }
        if (data.hasOwnProperty('nHp')) {
            genScreenMod("dmg");
        } else if (data.hasOwnProperty('pHp')) {
            genScreenMod("heal");
        }
        if (data.hasOwnProperty('shift')) {
            if (data.shift === 0) {
                STATE.game.shiftIcon.disable();
            }
            if (data.shift === 1) {
                STATE.game.shiftIcon.enable();
            }
        }
        if (data.hasOwnProperty('push')) {
            if (data.push === 0) {
                STATE.game.pushIcon.disable();
            }
            if (data.push === 1) {
                STATE.game.pushIcon.enable();
            }
        }
        if (data.hasOwnProperty('powerUps')) {
            for (let i in data.powerUps) {
                let x = data.powerUps[i].x;
                let y = data.powerUps[i].y;
                if (data.powerUps[i].hasOwnProperty('health')) {
                    STATE.game.powerUps[data.powerUps[i].health] = new PowerUp(x, y, 'health_power_up.svg');
                }
                if (data.powerUps[i].hasOwnProperty('shield')) {
                    STATE.game.powerUps[data.powerUps[i].shield] = new PowerUp(x, y, 'immunityFree.svg');
                }
                if (data.powerUps[i].hasOwnProperty('invisible')) {
                    STATE.game.powerUps[data.powerUps[i].invisible] = new PowerUp(x, y, 'invisiblePU.svg');
                }
            }
        }
        if (data.hasOwnProperty('toDel')) {
            console.log(JSON.stringify(data.toDel));
            for (let j in data.toDel) {
                if (STATE.game.powerUps[data.toDel[j]] !== undefined) {
                    STATE.graphics.pixiApp.stage.removeChild(STATE.game.powerUps[data.toDel[j]]);
                    STATE.game.powerUps[data.toDel[j]] = undefined;
                }
            }
        }
        if (STATE.game.encounterActive) {
            let rsp = STATE.game.controller.get();
            if (rsp.length > 0) {
                STATE.server.sendMessage(JSON.stringify({keycode: rsp}));
            } else {
                STATE.server.sendMessage("give");
            }
        }
    }
};


function endGreeting() {
    document.body.style.cursor = "default";
    STATE.game.playerName = STATE.game.keyLogger.value();
    STATE.graphics.pixiApp.stage.removeChildren();
    encounterInitialSetup();
    STATE.game.activeGreeting = false;
    let wait = new WaitingScreen("Waiting for players...", STATE.graphics.screenWidth / 2, STATE.graphics.screenHeight / 2, 50);
    STATE.graphics.appendGraphicalElement(wait);
}

function showTutorial() {
    document.body.style.cursor = "default";
    STATE.graphics.pixiApp.stage.removeChildren();
    STATE.game.activeGreeting = false;
    STATE.game.activeTutorial = true;
    console.log("SHOWT");
    STATE.graphics.appendGraphicalElement(new TutorialScreen());
}

window.onclick = (function f(event) {
    if (STATE.game.activeGreeting) {
        if(STATE.game.button.mouseIn(event.pageX, event.pageY)) {
            endGreeting();
        }
        if (STATE.game.tutorialButton.mouseIn(event.pageX, event.pageY)) {
            showTutorial();
        }
        return;
    }
    if (STATE.game.activeTutorial) {
        STATE.graphics.pixiApp.stage.removeChildren();
        STATE.game.activeTutorial = false;
        STATE.game.activeGreeting = true;
        roundInitialSetup();
        return;
    }
    if (STATE.game.encounterActive) {
        STATE.server.sendMessage(JSON.stringify({mX: event.pageX, mY: event.pageY}));
    }
});

window.onmousemove = function mouseMove(event) {
    if (STATE.hasOwnProperty('game') && STATE.game.hasOwnProperty('activeGreeting') && STATE.game.activeGreeting) {
        event = event || window.event;
        if (STATE.game.button.mouseIn(event.clientX, event.clientY)) {
            STATE.game.button.glow(true);
            document.body.style.cursor = "pointer";
        } else if (STATE.game.tutorialButton.mouseIn(event.clientX, event.clientY)) {
            STATE.game.tutorialButton.glow(true);
            document.body.style.cursor = "pointer";
        } else if (STATE.game.keyLogger.mouseIn(event.clientX, event.clientY)) {
            document.body.style.cursor = "text";
        } else {
            STATE.game.button.glow(false);
            STATE.game.tutorialButton.glow(false);
            document.body.style.cursor = "default";
        }
    }
};

let animate = function (delta) {
    if (STATE.graphics.loading !== undefined) {
        STATE.graphics.loading.rotation += (Math.PI / 36) * delta;
        return;
    }
    if (STATE.game.encounterActive) {
        for (let i in STATE.game.players) {
            STATE.game.players[i].move();
        }
        for (let i in STATE.game.saws) {
            STATE.game.saws[i].rotate();
        }
        let toRemove = [];

        for (let i in STATE.game.blows.children) {
            let curChild = STATE.game.blows.getChildAt(i);
            curChild.alpha *= 0.95;
            if (curChild.alpha < 0.1) {
                toRemove.push(curChild);
            }
        }
        for (let i in toRemove) {
            STATE.game.blows.removeChild(toRemove[i]);
        }
        if (STATE.game.dmg !== undefined) {
            STATE.game.dmg.alpha -= 0.05;
            if (STATE.game.dmg.alpha <= 0.05) {
                STATE.graphics.pixiApp.stage.removeChild(STATE.game.dmg);
                STATE.game.dmg = undefined;
            }
        }
        if (STATE.game.heal !== undefined) {
            STATE.game.heal.alpha -= 0.05;
            if (STATE.game.heal.alpha <= 0.05) {
                STATE.graphics.pixiApp.stage.removeChild(STATE.game.heal);
                STATE.game.heal = undefined;
            }
        }
    }
};


let STATE = {};

let type = "WebGL";
if (!PIXI.utils.isWebGLSupported()) {
    type = "canvas"
}


let roundInitialSetup = function () {
    let w = STATE.graphics.screenWidth, h = STATE.graphics.screenHeight;
    STATE.graphics.appendGraphicalElement(new GreetingScreen());
    STATE.game.keyLogger = new InputBox(w / 2, h / 2, h * 0.15, "Input your name:");
    STATE.graphics.appendGraphicalElement(STATE.game.keyLogger.graphical);
    STATE.game.button = new Button(w / 2, h * 3 / 4, h * 0.1, "playButton.svg");
    STATE.game.tutorialButton = new Button(w * 0.75, h * 0.75, h * 0.1, 'tutorialButton.svg');
    STATE.graphics.appendGraphicalElement(STATE.game.tutorialButton.graphical);
    STATE.graphics.appendGraphicalElement(STATE.game.button.graphical);
};

let encounterInitialSetup = function () {
    STATE.server.sendMessage(JSON.stringify({
        type: 'init',
        fieldWidth: STATE.graphics.screenWidth,
        fieldHeight: STATE.graphics.screenHeight,
        playerName: STATE.game.playerName
    }));
    console.log("Preparations finished.");
};


let Controller = function () {
    let up = false;
    let giveup = true;
    let left = false;
    let giveleft = true;
    let right = false;
    let giveright = true;
    let shift = false;
    let giveshift = true;

    let reg = function (key) {
        if (key === 65) {
            left = true;
        }
        if (key === -65) {
            left = false;
        }
        if (key === 68) {
            right = true;
        }
        if (key === -68) {
            right = false;
        }
        if (key === 32) {
            up = true;
        }
        if (key === -32) {
            up = false;
        }
        if (key === 16) {
            shift = true;
        }
        if (key === -16) {
            shift = false;
        }
    };

    let give = function () {
        let response = [];
        if (left && giveleft) {
            response.push(65);
            giveleft = false;
        } else if (!left && !giveleft) {
            response.push(-65);
            giveleft = true;
        }
        if (right && giveright) {
            response.push(68);
            giveright = false;
        } else if (!right && !giveright) {
            response.push(-68);
            giveright = true;
        }
        if (up && giveup) {
            response.push(32);
            giveup = false;
        } else if (!up && !giveup) {
            response.push(-32);
            giveup = true;
        }
        if (shift && giveshift) {
            response.push(16);
            giveshift = false;
        } else if (!shift && !giveshift) {
            response.push(-16);
            giveshift = true;
        }
        return response;
    };
    return {register: reg, get: give}
};

function main() {
    STATE.server.address = window.location.host + window.location.pathname;
    STATE.server.webSocketAddress = 'ws://' + STATE.server.address + 'handler';
    STATE.server.connection = new WebSocket(STATE.server.webSocketAddress);
    STATE.server.sendMessage = function (msg, callback) {
        if (callback === undefined) {
            callback = function () {
            }
        }
        if (!STATE.server.connection.readyState) {
            setTimeout(function () {
                STATE.server.sendMessage(msg, callback);
            }, 100);
        } else {
            STATE.server.connection.send(msg);
            callback();
        }
    };
    STATE.server.connection.onmessage = serverCallback;

    STATE.game = {};
    STATE.game.activeGreeting = true;
    STATE.game.activeMapFinding = false;
    STATE.game.encounterActive = false;
    STATE.game.endgame = false;
    STATE.game.playerName = '';
    STATE.game.blows = new PIXI.Container;
    STATE.game.powerUps = new PIXI.Container;
    STATE.game.resultMessage = '';
    STATE.game.players = [];
    STATE.game.saws = [];
    STATE.game.controller = new Controller();

    STATE.game.flush = function () {
        STATE.server.connection = new WebSocket(STATE.server.webSocketAddress);
        STATE.server.connection.onmessage = serverCallback;
        STATE.graphics.pixiApp.stage.removeChildren();
        STATE.game.blows = new PIXI.Container;
        STATE.game.endgame = false;
        STATE.game.activeGreeting = true;
        STATE.game.resultMessage = '';
        STATE.game.players = [];
        roundInitialSetup();
    };
    STATE.graphics.appendGraphicalElement = function (elem) {
        STATE.graphics.pixiApp.stage.addChild(elem);
    };
    STATE.graphics.accessImageTexture = function (img) {
        return STATE.graphics.loader.resources[STATE.server.pathToImages + img + ''].texture
    };
    STATE.graphics.Container = PIXI.Container;
    STATE.graphics.Text = PIXI.Text;
    STATE.graphics.TextStyle = PIXI.TextStyle;
    STATE.graphics.Sprite = PIXI.Sprite;
    STATE.graphics.loader = PIXI.loader;
    STATE.graphics.produceRect = function () {
        let rect = new PIXI.Graphics();
        rect.lineStyle(1, 0x000000, 1);
        return rect;
    };
    roundInitialSetup();
}


STATE.server = {};
STATE.server.pathToImages = "images/";

PIXI.loader
    .add(STATE.server.pathToImages + 'loading.svg')
    .load(function () {
        STATE.graphics = {};
        STATE.graphics.loading = true;
        STATE.graphics.canvas = document.getElementById("field");
        STATE.graphics.screenWidth = window.innerWidth - 4;
        STATE.graphics.screenHeight = window.innerHeight - 4;
        STATE.graphics.pixiApp = new PIXI.Application({
            view: STATE.graphics.canvas,
            antialias: true,
            transparent: false,
            backgroundColor: 0xFFFFFF,
            resolution: 1
        });
        STATE.graphics.pixiApp.renderer.autoResize = true;
        STATE.graphics.pixiApp.renderer.resize(STATE.graphics.screenWidth, STATE.graphics.screenHeight);
        STATE.graphics.loading = new PIXI.Sprite(PIXI.loader.resources[STATE.server.pathToImages + 'loading.svg'].texture);
        STATE.graphics.loading.anchor.x = 0.5;
        STATE.graphics.loading.anchor.y = 0.5;
        let loadSize = Math.min(STATE.graphics.screenWidth, STATE.graphics.screenHeight) /3;
        STATE.graphics.loading.width = loadSize;
        STATE.graphics.loading.height = loadSize;
        STATE.graphics.loading.x = (STATE.graphics.screenWidth) / 2;
        STATE.graphics.loading.y = (STATE.graphics.screenHeight) / 2;
        STATE.graphics.pixiApp.stage.addChild(STATE.graphics.loading);
        STATE.graphics.pixiApp.ticker.add(delta => animate(delta));
        PIXI.loader
            .add(STATE.server.pathToImages + 'stickman.svg')
            .add(STATE.server.pathToImages + 'leftleg.svg')
            .add(STATE.server.pathToImages + 'rightLeg.svg')
            .add(STATE.server.pathToImages + 'blow.svg')
            .add(STATE.server.pathToImages + 'back3.png')
            .add(STATE.server.pathToImages + 'back2.svg')
            .add(STATE.server.pathToImages + 'saw.svg')
            .add(STATE.server.pathToImages + 'dmg.svg')
            .add(STATE.server.pathToImages + 'heal.svg')
            .add(STATE.server.pathToImages + 'health_power_up.svg')
            .add(STATE.server.pathToImages + 'shiftingStickman.svg')
            .add(STATE.server.pathToImages + 'shift.svg')
            .add(STATE.server.pathToImages + 'push.svg')
            .add(STATE.server.pathToImages + 'immunityFree.svg')
            .add(STATE.server.pathToImages + 'immunity.svg')
            .add(STATE.server.pathToImages + 'invisiblePU.svg')
            .add(STATE.server.pathToImages + 'greeting.svg')
            .add(STATE.server.pathToImages + 'greetingTitle.svg')
            .add(STATE.server.pathToImages + 'input.svg')
            .add(STATE.server.pathToImages + 'playButton.svg')
            .add(STATE.server.pathToImages + 'glow_playButton.svg')
            .add(STATE.server.pathToImages + 'waitingBackground.svg')
            .add(STATE.server.pathToImages + 'waitingText.svg')
            .add(STATE.server.pathToImages + 'win.svg')
            .add(STATE.server.pathToImages + 'lose.svg')
            .add(STATE.server.pathToImages + 'tutorialButton.svg')
            .add(STATE.server.pathToImages + 'glow_tutorialButton.svg')
            .add(STATE.server.pathToImages + 'tutorial.svg')
            .load(function () {
                STATE.graphics.loading = undefined;
                STATE.graphics.pixiApp.stage.removeChildren();
                console.log("Textures loaded.");
                main();
            });
    });
