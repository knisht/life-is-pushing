function Player(x, y, width, height, name) {
    let hp = 3;
    let stickman = new STATE.graphics.Sprite(STATE.graphics.accessImageTexture("stickman.svg"));
    let shield = new STATE.graphics.Sprite(STATE.graphics.accessImageTexture("immunity.svg"));
    let shiftingStickman = new STATE.graphics.Sprite(STATE.graphics.accessImageTexture("shiftingStickman.svg"));
    let leftLeg = new STATE.graphics.Sprite(STATE.graphics.accessImageTexture("leftleg.svg"));
    let rightLeg = new STATE.graphics.Sprite(STATE.graphics.accessImageTexture("rightLeg.svg"));
    stickman.x = x;
    stickman.y = y;
    stickman.width = width;
    stickman.height = height;
    stickman.anchor.x = 0.5;

    shiftingStickman.width = width;
    shiftingStickman.height = height;
    shiftingStickman.anchor.x = 0.5;

    let shieldScaleFactor = Math.max(width, height) *1.5;
    shield.width = shieldScaleFactor;
    shield.height = shieldScaleFactor;
    shield.anchor.x = 0.5;
    shield.x = x - shieldScaleFactor/2;
    shield.y = y + shieldScaleFactor/2;
    shield.visible = false;
    leftLeg.x = x;
    leftLeg.y = y + stickman.height / 2;
    leftLeg.width = width;
    leftLeg.height = height;
    leftLeg.anchor.x = 0.5;
    leftLeg.anchor.y = 0.55;
    rightLeg.y = y + stickman.height / 2;
    rightLeg.width = width;
    rightLeg.height = height;
    rightLeg.anchor.x = 0.5;
    rightLeg.anchor.y = 0.55;
    let lastx = x;
    let lasty = y;
    let leftdelta = Math.PI / 18;
    let rightdelta = -Math.PI / 18;
    let direction = 1;
    let shift = false;


    let message = new STATE.graphics.Text(name, new STATE.graphics.TextStyle({
        fontFamily: "Arial",
        fontSize: 24,
        fill: "black",
        stroke: '#000000',
        strokeThickness: 1
    }));
    message.position.set(x + width / 2 - message.width / 2, y - 3);


    this.setPos = function (x, y) {
        lastx = x;
        lasty = y;
    };

    this.setName = function (newMsg) {
        message.text = newMsg;
    };

    this.setHp = function (newHp) {
        hp = newHp;
    };

    this.getHp = function () {
        return hp;
    };

    function rotateLegs() {
        if (leftLeg.rotation > 2 * Math.PI / 8 || leftLeg.rotation < -2 * Math.PI / 8) {
            leftdelta *= -1;
        }
        leftLeg.rotation += leftdelta;
        if (rightLeg.rotation > 2 * Math.PI / 8 || rightLeg.rotation < -2 * Math.PI / 8) {
            rightdelta *= -1;
        }
        rightLeg.rotation += rightdelta;
    }


    let ctain = new STATE.graphics.Container();
    ctain.addChild(stickman, message, leftLeg, rightLeg, shiftingStickman, shield);


    this.doMove = function () {
        if (!stickman.visible && !shiftingStickman.visible) {
            return;
        }
        if (!shift) {
            stickman.visible = true;
            shiftingStickman.visible = false;
            leftLeg.visible = true;
            leftLeg.visible = true;
        } else {
            stickman.visible = false;
            shiftingStickman.visible = true;
            leftLeg.visible = false;
            leftLeg.visible = false;
        }
        stickman.x += (lastx - stickman.x) * 0.25;
        stickman.y += (lasty - stickman.y) * 0.3;
        shiftingStickman.x = stickman.x;
        shiftingStickman.y = stickman.y;
        leftLeg.x = stickman.x;
        shield.x = stickman.x;
        shield.y = stickman.y - shieldScaleFactor*0.25;
        leftLeg.y = stickman.y + stickman.height * 0.55;
        rightLeg.x = stickman.x;
        rightLeg.y = stickman.y + stickman.height * 0.55;
        if (Math.abs(stickman.x - lastx) > 2 && Math.abs(stickman.y - lasty) <= 5) {
            rotateLegs();
        } else {
            leftLeg.rotation = 0;
            rightLeg.rotation = 0;
            leftdelta = Math.PI / 18;
            rightdelta = -Math.PI / 18;
        }
        if (Math.sign(lastx - stickman.x) !== direction) {
            direction *= -1;
            stickman.scale.x *= -1;
            shiftingStickman.scale.x *= -1;
            leftLeg.scale.x *= -1;
            rightLeg.scale.x *= -1;
        }
        message.x = stickman.x + width / 2 - message.width / 2;
        message.y = stickman.y - 20;
    };

    this.shft = function (type) {
        shift = type;
    };

    this.immn = function (type) {
        if (stickman.visible) {
            shield.visible = type;
        }
    };

    this.vis = function (type) {
        stickman.visible = type;
        shiftingStickman.visible = type;
        leftLeg.visible = type;
        rightLeg.visible = type;
        message.visible = type;
    };

    this.einvis = function (type) {
        let opacity = type ? 0.5 : 1;
        stickman.alpha = opacity;
        shiftingStickman.alpha = opacity;
        leftLeg.alpha = opacity;
        rightLeg.alpha = opacity;
    };

    return {
        graphical: ctain,
        relocate: this.setPos,
        rename: this.setName,
        renewHp: this.setHp,
        hp: this.getHp,
        move: this.doMove,
        setShift: this.shft,
        setBlowImmunity: this.immn,
        setVisible: this.vis,
        enableInvisibility: this.einvis
    }
}


function Saw(x, y, radius) {
    let saw = new STATE.graphics.Sprite(STATE.graphics.accessImageTexture("saw.svg"));
    saw.width = 2 * radius;
    saw.height = 2 * radius;
    saw.anchor.x = 0.5;
    saw.anchor.y = 0.5;
    saw.x = x;
    saw.y = y;

    function rot() {
        saw.rotation += Math.PI / 12;
    }

    return {graphical: saw, rotate: rot};
}

function HpBar(x, y, height, playerAmount) {
    let message = new STATE.graphics.Container();

    let cache = [];

    let style = new STATE.graphics.TextStyle({
        fontFamily: "Arial",
        fontSize: height,
        fill: "black",
        stroke: '#000000',
        strokeThickness: 1
    });
    for (let i = 0; i < playerAmount; ++i) {
        let txt = new PIXI.Text('3', style);
        message.addChild(txt);
        cache.push(txt);
        txt.position.set(x + (txt.width + 12) * i, y);
        if (i != playerAmount - 1) {
            let colon = new STATE.graphics.Text(":", style);
            colon.position.set(x + (txt.width + 12) * i + txt.width, y);
            message.addChild(colon);
        }
    }

    this.notf = function (text, index) {
        if (cache[index].text !== text) {
            cache[index].text = text;
        }
    };
    return {graphical: message, notify: this.notf}
}

function genBlow(x, y, angle) {
    let img = new STATE.graphics.Sprite(STATE.graphics.accessImageTexture('blow.svg'));
    STATE.game.blows.addChild(img);
    img.height = img.height * STATE.game.blowLim / img.width;
    img.width = STATE.game.blowLim;
    // console.log(STATE.game.blowLim);
    img.anchor.y = 0.5;
    img.x = x;
    img.y = y;
    img.rotation = angle;
}


function GreetingScreen() {
    let img = new STATE.graphics.Sprite(STATE.graphics.accessImageTexture('greeting.svg'));
    let title = new STATE.graphics.Sprite(STATE.graphics.accessImageTexture('greetingTitle.svg'));
    img.width = STATE.graphics.screenWidth;
    img.height = STATE.graphics.screenHeight;
    let scaleFactor = Math.min(STATE.graphics.screenHeight/ title.height, STATE.graphics.screenWidth / title.width) * 0.9;
    title.height = scaleFactor * title.height;
    title.width = scaleFactor * title.width;
    title.x = (STATE.graphics.screenWidth - title.width) / 2;
    let ctain = new STATE.graphics.Container();
    ctain.addChild(img, title);
    return ctain;
}


let Background = function (target) {
    let back = new STATE.graphics.Sprite(STATE.graphics.accessImageTexture(target));
    back.x = 0;
    back.y = 0;
    back.width = STATE.graphics.screenWidth;
    back.height = STATE.graphics.screenHeight;
    return back;
};

function Button(x, y, height, image) {
    let button = new STATE.graphics.Sprite(STATE.graphics.accessImageTexture(image));
    let glowButton = new STATE.graphics.Sprite(STATE.graphics.accessImageTexture('glow_' + image));
    let scaleFactor = height / button.height;
    button.width = button.width * scaleFactor;
    button.height = button.height * scaleFactor;
    button.position.set(x - button.width / 2, y);
    glowButton.width = glowButton.width * scaleFactor;
    glowButton.height = glowButton.height * scaleFactor;
    glowButton.position.set(x - glowButton.width / 2, y-3);
    glowButton.visible = false;

    this.mi = function (xx, yy) {
        return (xx >= x - button.width / 2 && yy >= y && xx <= x + button.width / 2 && yy <= y + height);
    };

    this.gl = function (flag) {
        glowButton.visible = flag;
    };

    let ctain = new STATE.graphics.Container();
    ctain.addChild(button, glowButton);
    return {graphical: ctain, mouseIn: this.mi, glow: this.gl};
}

 function InputBox(x, y, height, text) {
     let decoration = new STATE.graphics.Sprite(STATE.graphics.accessImageTexture('input.svg'));
     let scaleFactor = height / decoration.height;
     decoration.height = decoration.height * scaleFactor;
     decoration.width = decoration.width * scaleFactor;
     decoration.position.set(x - decoration.width / 2, y);


    let userInput = new STATE.graphics.Text('', new STATE.graphics.TextStyle({
        fontFamily: "Arial",
        fontSize: height * 0.3,
        fill: "black",
        stroke: '#000000',
        strokeThickness: 0
    }));
    userInput.position.set(x - userInput.width / 2, y + height * 0.45);

    this.rk = function (key) {
        if (key === 8) {
            userInput.text = userInput.text.substring(0, userInput.text.length - 1);
        } else if (key > 32) {
            let last = userInput.text;
            userInput.text = userInput.text + String.fromCharCode(key);
            if (userInput.width > decoration.width) {
                userInput.text = last;
            }
        }
        userInput.x = x - userInput.width/2 - 2;
    };

    this.val = function () {
        return userInput.text;
    };

    this.mi = function(xx, yy) {
        return (xx >= x - decoration.width / 2 && yy >= y + height * 0.5 && xx <= x + decoration.width / 2 && yy <= y + height);
    };

    let ctain = new STATE.graphics.Container;
    ctain.addChild(decoration, userInput);
    return {graphical: ctain, registerKey: this.rk, value: this.val, mouseIn: this.mi};
}

function genScreenMod(type) {
    if (type === "dmg") {
        let dmg = new STATE.graphics.Sprite(STATE.graphics.accessImageTexture("dmg.svg"));
        dmg.height = STATE.graphics.screenHeight;
        dmg.width = STATE.graphics.screenWidth;
        dmg.x = 0;
        dmg.y = 0;
        if (STATE.game.dmg!==undefined) {
            STATE.graphics.pixiApp.stage.removeChild(STATE.game.dmg);
        }
        STATE.game.dmg = dmg;
        STATE.graphics.appendGraphicalElement(dmg);
    } else if (type === "heal") {
        let dmg = new STATE.graphics.Sprite(STATE.graphics.accessImageTexture("heal.svg"));
        dmg.height = STATE.graphics.screenHeight;
        dmg.width = STATE.graphics.screenWidth;
        dmg.x = 0;
        dmg.y = 0;
        if (STATE.game.heal!==undefined) {
            STATE.graphics.pixiApp.stage.removeChild(STATE.game.heal);
        }
        STATE.game.heal = dmg;
        STATE.graphics.appendGraphicalElement(dmg);
    }
}

function PowerUp(x, y, src) {
    let img = new STATE.graphics.Sprite(STATE.graphics.accessImageTexture(src));
    let scaleFactor = Math.min(STATE.graphics.screenWidth / 15, STATE.graphics.screenHeight / 15);
    img.width = scaleFactor;
    img.height = scaleFactor;
    img.x = x - scaleFactor / 2;
    img.y = y - scaleFactor / 2;
    STATE.graphics.appendGraphicalElement(img);
    return img
}

function Icon(file, index) {
    let img = new STATE.graphics.Sprite(STATE.graphics.accessImageTexture(file));
    let scaleFactor = Math.min(STATE.graphics.screenWidth / 10, STATE.graphics.screenHeight / 10);
    img.width = scaleFactor;
    img.height = scaleFactor;
    img.x = 5 + index * img.width;
    img.y = STATE.graphics.screenHeight - img.height- 5;

    function enbl() {
        img.alpha = 1;
    }

    function dsbl(degree) {
        if (degree === undefined) {
            degree = 0.1;
        }
        img.alpha = degree;
    }

    return {graphical: img, enable: enbl, disable: dsbl}
}

function WaitingScreen() {

    let img = new STATE.graphics.Sprite(STATE.graphics.accessImageTexture('greeting.svg'));
    let title = new STATE.graphics.Sprite(STATE.graphics.accessImageTexture('waitingText.svg'));
    img.width = STATE.graphics.screenWidth;
    img.height = STATE.graphics.screenHeight;
    let scaleFactor = Math.min(STATE.graphics.screenHeight/ title.height, STATE.graphics.screenWidth / title.width);
    title.height = scaleFactor * title.height;
    title.width = scaleFactor * title.width;
    title.x = (STATE.graphics.screenWidth - title.width) / 2;
    let ctain = new STATE.graphics.Container();
    ctain.addChild(img, title);
    return ctain;
}

function EndgameScreen(image) {

    let img = new STATE.graphics.Sprite(STATE.graphics.accessImageTexture('greeting.svg'));
    let title = new STATE.graphics.Sprite(STATE.graphics.accessImageTexture(image));
    img.width = STATE.graphics.screenWidth;
    img.height = STATE.graphics.screenHeight;
    let scaleFactor = Math.min(STATE.graphics.screenHeight/ title.height, STATE.graphics.screenWidth / title.width);
    title.height = scaleFactor * title.height;
    title.width = scaleFactor * title.width;
    title.x = (STATE.graphics.screenWidth - title.width) / 2;
    let ctain = new STATE.graphics.Container();
    ctain.addChild(img, title);
    return ctain;
}

function TutorialScreen() {

    let img = new STATE.graphics.Sprite(STATE.graphics.accessImageTexture('greeting.svg'));
    let title = new STATE.graphics.Sprite(STATE.graphics.accessImageTexture('tutorial.svg'));
    img.width = STATE.graphics.screenWidth;
    img.height = STATE.graphics.screenHeight;
    let scaleFactor = Math.min(STATE.graphics.screenHeight/ title.height, STATE.graphics.screenWidth / title.width);
    title.height = scaleFactor * title.height;
    title.width = scaleFactor * title.width;
    title.x = (STATE.graphics.screenWidth - title.width) / 2;
    let ctain = new STATE.graphics.Container();
    ctain.addChild(img, title);
    return ctain;
}